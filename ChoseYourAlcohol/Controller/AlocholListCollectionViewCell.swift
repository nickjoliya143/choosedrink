//
//  AlocholListCollectionViewCell.swift
//  ChoseYourAlcohol
//
//  Created by mac on 01/02/23.
//

import UIKit

class AlocholListCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgAlcohol: UIImageView!
}
