//
//  ViewController.swift
//  ChoseYourAlcohol
//
//  Created by mac on 01/02/23.
//

import UIKit

class WelcomeVC: UIViewController ,UITextFieldDelegate{

    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var txtName: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationItem.backButtonTitle = ""
        btnNext.isHidden = true
        txtName.delegate = self
    }

    @IBAction func btnNext(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainVC") as! MainVC
        vc.userName = txtName.text ?? "User"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        btnNext.isHidden = false
    }

}

