//
//  DetailedVC.swift
//  ChoseYourAlcohol
//
//  Created by mac on 01/02/23.
//

import UIKit
import SDWebImage
import KRProgressHUD

class DetailedVC: UIViewController {
    
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblCatagory: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgDrink: UIImageView!
    var drinkID = ""
    var arrAlcoholList = [DrinkData]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        callAPI(urlString: "https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=\(drinkID)")
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func callAPI(urlString:String){
        
        let url = URL(string: urlString)!
        KRProgressHUD.show()
        let _: Void = URLSession.shared.dataTask(with: url) { [self] (data, response, error) in
            guard let data = data else{return}
            print(data)
            do{
                //error bad eccess
                let postData = try JSONDecoder().decode(DrinkModel.self, from: data)
                print(postData)
                KRProgressHUD.dismiss()
                DispatchQueue.main.async { [self] in
                    arrAlcoholList.append(contentsOf: postData.drinks ?? [])
                    lblTitle.text = arrAlcoholList[0].strDrink
                    lblDescription.text = arrAlcoholList[0].strInstructions
                    lblCatagory.text = arrAlcoholList[0].strCategory
                    imgDrink.sd_setImage(with: URL(string: arrAlcoholList[0].strDrinkThumb ?? ""))
                    
                }
               
                
            }
            catch{
                print(error.localizedDescription)
            }
        }.resume()
    }
}
