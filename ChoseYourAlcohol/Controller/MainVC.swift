//
//  MainVC.swift
//  ChoseYourAlcohol
//
//  Created by mac on 02/02/23.
//

import UIKit
import CarbonKit

class MainVC: UIViewController,CarbonTabSwipeNavigationDelegate{

    @IBOutlet weak var carbanView: UIView!
    var Carbon = CarbonTabSwipeNavigation()
    var arrListOfTab = ["All" ,"Ordinary Drink", "Cocktail"]
    var userName = ""
    @IBOutlet weak var btnUser: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

       
        btnUser.setTitle(userName, for: .normal)
        Carbon = CarbonTabSwipeNavigation(items: arrListOfTab, delegate: self)
        Carbon.carbonSegmentedControl?.setWidth(view.frame.size.width / 3, forSegmentAt: 0)
        Carbon.carbonSegmentedControl?.setWidth(view.frame.size.width / 3, forSegmentAt: 1)
        Carbon.carbonSegmentedControl?.setWidth(view.frame.size.width / 3, forSegmentAt: 2)
        Carbon.carbonSegmentedControl?.backgroundColor = UIColor.black
        Carbon.setSelectedColor(UIColor.white,font: UIFont.systemFont(ofSize: 16))
        Carbon.setNormalColor(UIColor.lightGray,font: UIFont.systemFont(ofSize: 15))
        Carbon.setIndicatorColor(UIColor.white)
        Carbon.insert(intoRootViewController: self, andTargetView: carbanView)
        Carbon.setTabBarHeight(50)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.backButtonTitle = ""
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
         
            if index == 0
            {
                return (self.storyboard?.instantiateViewController(withIdentifier: "AlcoholListVC")) as! AlcoholListVC
            }
            else if index == 1
            {
            return (self.storyboard?.instantiateViewController(withIdentifier: "OrdinaryDrinkVC")) as! OrdinaryDrinkVC
            }
            
            else
            {
                return (self.storyboard?.instantiateViewController(withIdentifier: "CocktailVC")) as! CocktailVC
            }
        
    }
    
    
    
    @IBAction func btnUser(_ sender: UIButton) {
        
        
    }
    
}
