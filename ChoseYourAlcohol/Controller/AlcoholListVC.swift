//
//  AlcoholListVC.swift
//  ChoseYourAlcohol
//
//  Created by mac on 01/02/23.
//

import UIKit
import SDWebImage
import KRProgressHUD

class AlcoholListVC: UIViewController {

    @IBOutlet weak var alcoholCollectionView: UICollectionView!
    
    var arrAlcoholList = [DrinkData]()
    var arrABC = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        alcoholCollectionView.delegate = self
        alcoholCollectionView.dataSource   = self
        
       // self.title = "YourDrink"
       // self.navigationItem.backButtonTitle = ""
        
        for i in 0...arrABC.count - 1{
            
            callAPI(urlString: "https://www.thecocktaildb.com/api/json/v1/1/search.php?f=\(arrABC[i])")
        }
        
       
       
        
        
        //callAPI(urlString: "https://www.thecocktaildb.com/api/json/v1/1/search.php?f=c")
    }

    func callAPI(urlString:String){
        
        let url = URL(string: urlString)!
        KRProgressHUD.show()
        let _: Void = URLSession.shared.dataTask(with: url) { [self] (data, response, error) in
            guard let data = data else{return}
            print(data)
            do{
                //error bad eccess
                let postData = try JSONDecoder().decode(DrinkModel.self, from: data)
                print(postData)
                KRProgressHUD.dismiss()
                DispatchQueue.main.async {
                    
                    self.arrAlcoholList.append(contentsOf: postData.drinks ?? [])
                    self.arrAlcoholList = self.arrAlcoholList.sorted { $0.strDrink ?? "a" < $1.strDrink ?? "b" }
                    self.alcoholCollectionView.reloadData()
                }
               
                
            }
            catch{
                print(error.localizedDescription)
            }
        }.resume()
    }
}

extension AlcoholListVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrAlcoholList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = alcoholCollectionView.dequeueReusableCell(withReuseIdentifier: "AlocholListCollectionViewCell", for: indexPath) as! AlocholListCollectionViewCell
        
        cell.lblTitle.text = arrAlcoholList[indexPath.row].strDrink
        cell.imgAlcohol.sd_setImage(with: URL(string: arrAlcoholList[indexPath.row].strDrinkThumb ?? ""))
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.frame.size.width / 2
        
        return CGSize(width: width, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DetailedVC") as! DetailedVC
        
        vc.drinkID = arrAlcoholList[indexPath.row].idDrink ?? ""
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}
