//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct DrinkModel : Codable {

	let drinks : [DrinkData]?


	enum CodingKeys: String, CodingKey {
		case drinks = "drinks"
	}
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		drinks = try values.decodeIfPresent([DrinkData].self, forKey: .drinks)
	}


}


struct DrinkData : Codable {

    let dateModified : String?
    let idDrink : String?
    let strAlcoholic : String?
    let strCategory : String?
    let strCreativeCommonsConfirmed : String?
    let strDrink : String?
    let strDrinkAlternate : String?
    let strDrinkThumb : String?
    let strGlass : String?
    let strIBA : String?
    let strImageAttribution : String?
    let strImageSource : String?
    let strIngredient1 : String?
    let strIngredient10 : String?
    let strIngredient11 : String?
    let strIngredient12 : String?
    let strIngredient13 : String?
    let strIngredient14 : String?
    let strIngredient15 : String?
    let strIngredient2 : String?
    let strIngredient3 : String?
    let strIngredient4 : String?
    let strIngredient5 : String?
    let strIngredient6 : String?
    let strIngredient7 : String?
    let strIngredient8 : String?
    let strIngredient9 : String?
    let strInstructions : String?
    let strInstructionsDE : String?
    let strInstructionsES : String?
    let strInstructionsFR : String?
    let strInstructionsIT : String?
    let strInstructionsZHHANS : String?
    let strInstructionsZHHANT : String?
    let strMeasure1 : String?
    let strMeasure10 : String?
    let strMeasure11 : String?
    let strMeasure12 : String?
    let strMeasure13 : String?
    let strMeasure14 : String?
    let strMeasure15 : String?
    let strMeasure2 : String?
    let strMeasure3 : String?
    let strMeasure4 : String?
    let strMeasure5 : String?
    let strMeasure6 : String?
    let strMeasure7 : String?
    let strMeasure8 : String?
    let strMeasure9 : String?
    let strTags : String?
    let strVideo : String?


    enum CodingKeys: String, CodingKey {
        case dateModified = "dateModified"
        case idDrink = "idDrink"
        case strAlcoholic = "strAlcoholic"
        case strCategory = "strCategory"
        case strCreativeCommonsConfirmed = "strCreativeCommonsConfirmed"
        case strDrink = "strDrink"
        case strDrinkAlternate = "strDrinkAlternate"
        case strDrinkThumb = "strDrinkThumb"
        case strGlass = "strGlass"
        case strIBA = "strIBA"
        case strImageAttribution = "strImageAttribution"
        case strImageSource = "strImageSource"
        case strIngredient1 = "strIngredient1"
        case strIngredient10 = "strIngredient10"
        case strIngredient11 = "strIngredient11"
        case strIngredient12 = "strIngredient12"
        case strIngredient13 = "strIngredient13"
        case strIngredient14 = "strIngredient14"
        case strIngredient15 = "strIngredient15"
        case strIngredient2 = "strIngredient2"
        case strIngredient3 = "strIngredient3"
        case strIngredient4 = "strIngredient4"
        case strIngredient5 = "strIngredient5"
        case strIngredient6 = "strIngredient6"
        case strIngredient7 = "strIngredient7"
        case strIngredient8 = "strIngredient8"
        case strIngredient9 = "strIngredient9"
        case strInstructions = "strInstructions"
        case strInstructionsDE = "strInstructionsDE"
        case strInstructionsES = "strInstructionsES"
        case strInstructionsFR = "strInstructionsFR"
        case strInstructionsIT = "strInstructionsIT"
        case strInstructionsZHHANS = "strInstructionsZH-HANS"
        case strInstructionsZHHANT = "strInstructionsZH-HANT"
        case strMeasure1 = "strMeasure1"
        case strMeasure10 = "strMeasure10"
        case strMeasure11 = "strMeasure11"
        case strMeasure12 = "strMeasure12"
        case strMeasure13 = "strMeasure13"
        case strMeasure14 = "strMeasure14"
        case strMeasure15 = "strMeasure15"
        case strMeasure2 = "strMeasure2"
        case strMeasure3 = "strMeasure3"
        case strMeasure4 = "strMeasure4"
        case strMeasure5 = "strMeasure5"
        case strMeasure6 = "strMeasure6"
        case strMeasure7 = "strMeasure7"
        case strMeasure8 = "strMeasure8"
        case strMeasure9 = "strMeasure9"
        case strTags = "strTags"
        case strVideo = "strVideo"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        dateModified = try values.decodeIfPresent(String.self, forKey: .dateModified)
        idDrink = try values.decodeIfPresent(String.self, forKey: .idDrink)
        strAlcoholic = try values.decodeIfPresent(String.self, forKey: .strAlcoholic)
        strCategory = try values.decodeIfPresent(String.self, forKey: .strCategory)
        strCreativeCommonsConfirmed = try values.decodeIfPresent(String.self, forKey: .strCreativeCommonsConfirmed)
        strDrink = try values.decodeIfPresent(String.self, forKey: .strDrink)
        strDrinkAlternate = try values.decodeIfPresent(String.self, forKey: .strDrinkAlternate)
        strDrinkThumb = try values.decodeIfPresent(String.self, forKey: .strDrinkThumb)
        strGlass = try values.decodeIfPresent(String.self, forKey: .strGlass)
        strIBA = try values.decodeIfPresent(String.self, forKey: .strIBA)
        strImageAttribution = try values.decodeIfPresent(String.self, forKey: .strImageAttribution)
        strImageSource = try values.decodeIfPresent(String.self, forKey: .strImageSource)
        strIngredient1 = try values.decodeIfPresent(String.self, forKey: .strIngredient1)
        strIngredient10 = try values.decodeIfPresent(String.self, forKey: .strIngredient10)
        strIngredient11 = try values.decodeIfPresent(String.self, forKey: .strIngredient11)
        strIngredient12 = try values.decodeIfPresent(String.self, forKey: .strIngredient12)
        strIngredient13 = try values.decodeIfPresent(String.self, forKey: .strIngredient13)
        strIngredient14 = try values.decodeIfPresent(String.self, forKey: .strIngredient14)
        strIngredient15 = try values.decodeIfPresent(String.self, forKey: .strIngredient15)
        strIngredient2 = try values.decodeIfPresent(String.self, forKey: .strIngredient2)
        strIngredient3 = try values.decodeIfPresent(String.self, forKey: .strIngredient3)
        strIngredient4 = try values.decodeIfPresent(String.self, forKey: .strIngredient4)
        strIngredient5 = try values.decodeIfPresent(String.self, forKey: .strIngredient5)
        strIngredient6 = try values.decodeIfPresent(String.self, forKey: .strIngredient6)
        strIngredient7 = try values.decodeIfPresent(String.self, forKey: .strIngredient7)
        strIngredient8 = try values.decodeIfPresent(String.self, forKey: .strIngredient8)
        strIngredient9 = try values.decodeIfPresent(String.self, forKey: .strIngredient9)
        strInstructions = try values.decodeIfPresent(String.self, forKey: .strInstructions)
        strInstructionsDE = try values.decodeIfPresent(String.self, forKey: .strInstructionsDE)
        strInstructionsES = try values.decodeIfPresent(String.self, forKey: .strInstructionsES)
        strInstructionsFR = try values.decodeIfPresent(String.self, forKey: .strInstructionsFR)
        strInstructionsIT = try values.decodeIfPresent(String.self, forKey: .strInstructionsIT)
        strInstructionsZHHANS = try values.decodeIfPresent(String.self, forKey: .strInstructionsZHHANS)
        strInstructionsZHHANT = try values.decodeIfPresent(String.self, forKey: .strInstructionsZHHANT)
        strMeasure1 = try values.decodeIfPresent(String.self, forKey: .strMeasure1)
        strMeasure10 = try values.decodeIfPresent(String.self, forKey: .strMeasure10)
        strMeasure11 = try values.decodeIfPresent(String.self, forKey: .strMeasure11)
        strMeasure12 = try values.decodeIfPresent(String.self, forKey: .strMeasure12)
        strMeasure13 = try values.decodeIfPresent(String.self, forKey: .strMeasure13)
        strMeasure14 = try values.decodeIfPresent(String.self, forKey: .strMeasure14)
        strMeasure15 = try values.decodeIfPresent(String.self, forKey: .strMeasure15)
        strMeasure2 = try values.decodeIfPresent(String.self, forKey: .strMeasure2)
        strMeasure3 = try values.decodeIfPresent(String.self, forKey: .strMeasure3)
        strMeasure4 = try values.decodeIfPresent(String.self, forKey: .strMeasure4)
        strMeasure5 = try values.decodeIfPresent(String.self, forKey: .strMeasure5)
        strMeasure6 = try values.decodeIfPresent(String.self, forKey: .strMeasure6)
        strMeasure7 = try values.decodeIfPresent(String.self, forKey: .strMeasure7)
        strMeasure8 = try values.decodeIfPresent(String.self, forKey: .strMeasure8)
        strMeasure9 = try values.decodeIfPresent(String.self, forKey: .strMeasure9)
        strTags = try values.decodeIfPresent(String.self, forKey: .strTags)
        strVideo = try values.decodeIfPresent(String.self, forKey: .strVideo)
    }


}
